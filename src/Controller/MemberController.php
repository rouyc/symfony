<?php

namespace App\Controller;

use App\Entity\Member;
use App\Form\MemberType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MemberController extends AbstractController
{
    /**
     * @Route("/member", name="member")
     */
    public function index(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Member::class);
        return $this->render('member/member.html.twig', [
            'members' => $repository->findAll()
        ]);
    }

    /**
     * @Route("/member/admin", name="member_admin")
     */
    public function memberAdmin(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Member::class);
        return $this->render('member/memberadmin.html.twig', [
            'members' => $repository->findAll()
        ]);
    }

    /**
     * Crée un nouveau groupe
     *
     * @param Request $request
     *
     * @Route("/member/admin/create", name="member_create", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function memberCreate(Request $request)
    {
        $member = new Member();

        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $member = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($member);
            $entityManager->flush();

            return $this->redirectToRoute('member_admin');
        }
        return $this->render('member/form.html.twig', [
            'form_title' => "Créer un membre",
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Member $member
     *
     * @return Response
     * @Route("/member/admin/delete/{id}", name="member_delete", methods={"GET","DELETE"})
     * @isGranted("ROLE_ADMIN")
     */
    public function memberDelete(request $request, Member $member): Response
    {
        $entityManager = $this-> getDoctrine()->getManager();
        $entityManager->remove($member);
        $entityManager->flush();

        return $this->redirectToRoute('member_admin');
    }

    /**
     * @param Request $request
     * @param Member $member
     *
     * @return Response
     * @Route("/member/admin/update/{id}", name="member_update", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function memberUpdate(request $request, Member $member): Response
    {
        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $member = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($member);
            $entityManager->flush();

            return $this->redirectToRoute('member_admin');
        }

        return $this->render('member/form.html.twig', [
            'form_title' => "Modifier un membre",
            'form' => $form->createView()
        ]);
    }
}

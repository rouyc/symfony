<?php

namespace App\Controller;

use App\Form\ShowType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use App\Entity\Show;

class ShowController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Show::class);
        return $this->render('concert/show.html.twig', [
            'message' => 'Pour accéder au reste du site vous devez avoir un compte avec un ROLE_USER, pour accéder au page de modification des différents groupes / membres / concerts il faut un ROLE_ADMIN',
            'type' => 'Les 10 prochains concerts',
            'concerts' => $repository->find10next()
        ]);
    }

    /**
     * @Route("/concert", name="concert")
     */
    public function concertList(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Show::class);
        return $this->render('concert/show.html.twig', [
            'message' => '',
            'type' => 'Tous les concerts',
            'concerts' => $repository->findnext()
        ]);
    }

    /**
     * @Route("/concert/past", name="concert_past")
     */
    public function concertPast(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Show::class);
        return $this->render('concert/show.html.twig', [
            'message' => '',
            'type' => 'Concert passée',
            'concerts' => $repository->findpast()
        ]);
    }

    /**
     * @Route("/concert/admin", name="concert_admin")
     */
    public function concertAdmin(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Show::class);
        return $this->render('concert/showadmin.html.twig', [
            'type' => 'Concert passée',
            'concerts' => $repository->findpast()
        ]);
    }

    /**
     *
     * Crée un nouveau concert
     * @Route("/concert/admin/create", name="concert_create", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createConcert(Request $request)
    {
        $concert = new Show();

        $form = $this->createForm(ShowType::class, $concert);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $concert = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($concert);
            $entityManager->flush();

            return $this->redirectToRoute('concert_admin');
        }
        return $this->render('concert/form.html.twig', [
            'form_title' => "Ajouter un concert",
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Show $concert
     *
     * @return Response
     * @Route("/concert/admin/delete/{id}", name="concert_delete", methods={"GET","DELETE"})
     * @isGranted("ROLE_ADMIN")
     */
    public function concertDelete(request $request, Show $concert): Response
    {
        $entityManager = $this-> getDoctrine()->getManager();
        $entityManager->remove($concert);
        $entityManager->flush();

        return $this->redirectToRoute('concert_admin');
    }

    /**
     * @param Request $request
     * @param Show $concert
     *
     * @return Response
     * @Route("/concert/admin/update/{id}", name="concert_update", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function concertUpdate(request $request, Show $concert): Response
    {
        $form = $this->createForm(ShowType::class, $concert);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $concert = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($concert);
            $entityManager->flush();

            return $this->redirectToRoute('concert_admin');
        }

        return $this->render('concert/form.html.twig', [
            'form_title' => "Modifier un concert",
            'form' => $form->createView()
        ]);
    }
}

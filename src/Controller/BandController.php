<?php

namespace App\Controller;


use App\Form\BandType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Band;

class BandController extends AbstractController
{
    /**
     * Affiche une liste de groupes de la database
     *
     * @Route("/bands", name="band")
     *
     */
    public function bandsAll(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Band::class);
        $bands = $repository->findAll();
        return $this->render('band/band.html.twig', [
                'bands' => $bands
            ]
        );
    }

    /**
     * Affiche une liste de groupes de la database
     *
     * @Route("/band/admin", name="band_admin")
     *
     */
    public function bandAdmin(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Band::class);
        $bands = $repository->findAll();
        return $this->render('band/bandadmin.html.twig', [
                'bands' => $bands
            ]
        );
    }

    /**
     * Crée un nouveau groupe
     *
     * @param Request $request
     *
     * @Route("/band/admin/create", name="band_create", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function bandCreate(Request $request)
    {
        $band = new Band();

        $form = $this->createForm(BandType::class, $band);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $member = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($member);
            $entityManager->flush();

            return $this->redirectToRoute('band_admin');
        }
        return $this->render('band/form.html.twig', [
            'form_title' => "Créer un groupe",
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Band $band
     * @return Response
     * @Route("/band/admin/delete/{id}", name="band_delete", methods={"GET","DELETE"})
     * @isGranted("ROLE_ADMIN")
     */
    public function bandDelete(Band $band): Response
    {
        $entityManager = $this-> getDoctrine()->getManager();
        $entityManager->remove($band);
        $entityManager->flush();

        return $this->redirectToRoute('band_admin');
    }

    /**
     * @param Request $request
     *
     * @param Band $band
     * @return Response
     * @Route("/band/admin/update/{id}", name="band_update", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function bandUpdate(request $request, Band $band): Response
    {
        $form = $this->createForm(BandType::class, $band);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $band = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($band);
            $entityManager->flush();

            return $this->redirectToRoute('band_admin');
        }

        return $this->render('band/form.html.twig', [
            'form_title' => "Modifier un groupe",
            'form' => $form->createView()
        ]);
    }

    /**
     * Affiche un groupe en particulier
     *
     * @param int $id
     *
     * @Route("/band/{id}", name="band_id")
     */
    public function bandId(int $id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Band::class);
        return $this->render('band/index.html.twig', [
                'band' => $repository->find($id)
            ]
        );
    }
}

<?php

namespace App\Repository;

use App\Entity\Show;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Show|null find($id, $lockMode = null, $lockVersion = null)
 * @method Show|null findOneBy(array $criteria, array $orderBy = null)
 * @method Show[]    findAll()
 * @method Show[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Show::class);
    }

    /**
    * Return 10 futures concerts
    */
    public function find10next()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.date >= :now')
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('s.date', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    /**
     * Return futures concerts
     */
    public function findnext()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.date >= :now')
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('s.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Return past concerts
     */
    public function findpast()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.date < :now')
            ->setParameter('now', new \DateTime('now'))
            ->orderBy('s.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Show
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Form;

use App\Entity\Band;
use App\Entity\Member;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'Nom du groupe'
            ])
            ->add('style', TextType::class,[
                'label' => 'Style'
            ])
            ->add('creationYear', DateType::class,[
                'label' => 'Date de création',
                'widget' => 'choice',
                'format' => 'dd / MM / yyyy'
            ])
            ->add('lastAlbumName', TextType::class,[
                'label' => 'Nom du groupe'
            ])
            ->add('members', EntityType::class,[
                'class' => Member::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'save'],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Band::class,
        ]);
    }
}

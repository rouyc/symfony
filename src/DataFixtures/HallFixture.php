<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Hall;

class HallFixture extends Fixture
{
    public const H_1 = 'h1';
    public const H_2 = 'h2';
    public const H_3 = 'h3';

    public function load(ObjectManager $manager)
    {
        $h1 = new Hall();
        $h1->setName('HALL 1')
            ->setCapacity(350)
            ->setAvailable(true);
        $manager->persist($h1);

        $h2 = new Hall();
        $h2->setName('HALL 2')
            ->setCapacity(2000)
            ->setAvailable(false);
        $manager->persist($h2);

        $h3 = new Hall();
        $h3->setName('HALL 3')
            ->setCapacity(10000)
            ->setAvailable(true);
        $manager->persist($h3);

        $manager->flush();

        $this->addReference(self::H_1, $h1);
        $this->addReference(self::H_2, $h2);
        $this->addReference(self::H_3, $h3);
    }
}

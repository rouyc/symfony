<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Band;

class BandFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $b1 = new Band();
        $b1->setName('Sexion Assault')
            ->setStyle('Rap')
            ->setPicture('sexionassault.jpg')
            ->setCreationYear(new \DateTime(2002))
            ->setLastAlbumName('Apogée')
            ->addMember($this->getReference(MemberFixture::SA_1))
            ->addMember($this->getReference(MemberFixture::SA_2))
            ->addMember($this->getReference(MemberFixture::SA_3));

        $manager->persist($b1);

        $b2 = new Band();
        $b2->setName('Jul')
            ->setStyle('Rap')
            ->setPicture('jul.jpg')
            ->setCreationYear(new \DateTime(2010))
            ->setLastAlbumName('DP')
            ->addMember($this->getReference(MemberFixture::J_1));

        $manager->persist($b2);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            MemberFixture::class,
        );
    }
}
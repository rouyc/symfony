<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\ConcertHall;

class ConcertHallFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $ch1 = new ConcertHall();
        $ch1->setName('Triple Room')
            ->setTotalCapacity(3000)
            ->setPresentation("Welcome to my ConcertHall")
            ->setCity('Montpellier')
            ->addHall($this->getReference(HallFixture::H_1))
            ->addHall($this->getReference(HallFixture::H_2))
            ->addHall($this->getReference(HallFixture::H_3));
        $manager->persist($ch1);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            HallFixture::class,
        );
    }
}

<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Member;

class MemberFixture extends Fixture
{
    public const SA_1 = 'm1';
    public const SA_2 = 'm2';
    public const SA_3 = 'm3';
    public const J_1 = 'm4';

    public function load(ObjectManager $manager)
    {
        $m1 = new Member();
        $m1->setName('Karim')
            ->setFirstName('Fall')
            ->setjob('Chanteur')
            ->setBirthDate(\DateTime::createFromFormat("d/m/Y", '28/11/1985'))
            ->setPicture('lefa.jpg');
        $manager->persist($m1);

        $m2 = new Member();
        $m2->setName('Alpha')
            ->setFirstName('Diallo')
            ->setjob('Chanteur')
            ->setBirthDate(\DateTime::createFromFormat("d/m/Y", '27/12/1984'))
            ->setPicture('blackm.jpg');
        $manager->persist($m2);

        $m3 = new Member();
        $m3->setName('Gandhi')
            ->setFirstName('Djuna')
            ->setjob('Chanteur')
            ->setBirthDate(\DateTime::createFromFormat("d/m/Y", '05/05/1986'))
            ->setPicture('maitregims.jpg');
        $manager->persist($m3);

        $m4 = new Member();
        $m4->setName('JUL')
            ->setFirstName('JUL')
            ->setjob('Chanteur')
            ->setBirthDate(\DateTime::createFromFormat("d/m/Y", '01/01/2000'))
            ->setPicture('jul.jpg');
        $manager->persist($m4);

        $manager->flush();

        $this->addReference(self::SA_1, $m1);
        $this->addReference(self::SA_2, $m2);
        $this->addReference(self::SA_3, $m3);
        $this->addReference(self::J_1, $m4);
    }
}
